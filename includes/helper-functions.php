<?php

if( !function_exists( 'wext_get_config_value' ) ){
    /**
     * getting Config value. If get config value from post, then it will receive from post, Otherwise, will take data from Configuration value.
     * 
     * @param type $table_ID Mainly post ID of wpt_product_table. That means: its post id of product table
     * @return type Array
     */
    function wext_get_config_value( $table_ID ){
        $config_value = '';
        $config = get_post_meta( $table_ID, 'wext_config', true );        
        $config = is_array( $config ) ? array_filter( $config ) : array();
        if( !empty( $config ) && is_array( $config ) && is_array( $config_value ) ){
            $config_value = array_merge( $config_value, $config );
        }
        $config_value = apply_filters( 'wext_get_config_value', $config_value, $table_ID );
        /*
        $config_value['footer_cart'] = $temp_config_value['footer_cart'];
        $config_value['footer_cart_size'] = $temp_config_value['footer_cart_size'];
        $config_value['footer_possition'] = $temp_config_value['footer_possition'];
        $config_value['footer_bg_color'] = $temp_config_value['footer_bg_color'];
        //$config_value['thumbs_lightbox'] = $temp_config_value['thumbs_lightbox'];
        $config_value['disable_cat_tag_link'] = $temp_config_value['disable_cat_tag_link'];
        */
        return 263;
    }
}

if( !function_exists( 'wext_add_custom_message_field' ) ){
    /**
     * Adding Custom Mesage Field in Single Product Page
     * By Default: Disable, if you need, you can active it by enable action under this function
     * 
     * @since 1.9
     * @date 7.6.2018 d.m.y
     */
    function wext_add_custom_message_field() {
        echo '<table class="variations" cellspacing="0">
              <tbody>
                  <tr>
                  <td class="label"><label for="custom_message">'.esc_html__( 'Message', 'wpt_pro' ).'</label></td>
                  <td class="value">
                      <input id="custom_message" type="text" name="wpt_custom_message" placeholder="'.esc_attr__( 'Short Message for Order', 'woo-extender-table' ).'" />                      
                  </td>
              </tr>                               
              </tbody>
          </table>';
    }
}