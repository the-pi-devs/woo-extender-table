<?php

global $Ext_ShortCode_Text;

add_shortcode( $Ext_ShortCode_Text, 'wext_shortcode_generator' );


if( !function_exists( 'wext_shortcode_generator' ) ){

    /**
     * Shortcode Generator for this plugin output.
     *
     * @package Woo Extender Table
     * @version 1.0.0
     * @since   1.0.0
     * @param array $atts
     * @return string
     */

    function wext_shortcode_generator( $atts = false ) {

        //Getting Table
        $table_show = apply_filters( 'wext_table_show_top', true, $atts );
        if ( ! $table_show ){
            return;
        }

        $config_value = '';
        $_device = '';
        $html = '';

        $GLOBALS['wext_product_table'] = 'Yes';

        /**
         * Set Variable $html to return
         *@package Woo Extender Table
        * @version 1.0.0
        * @since   1.0.0
         */

         $pairs = array('exclude' =>  false );

         extract( shortcode_atts( $pairs, $atts ) );

         $atts_id = isset( $atts['id'] ) && !empty( $atts['id'] ) ? (int) $atts['id'] : 0;

         $table_status = get_post_status( $atts_id );

         if ( $atts_id && get_post_type( $atts_id ) == 'wext_product_table' && $table_status == 'publish' ) {
             $ID = $table_ID = (int) $atts['id'];
             $GLOBALS['wext_product_table'] = $ID;
             $_device = $ID;

             $enabled_column_array = get_post_meta( $ID, 'wext_enabled_column_array', true );

             if ( empty( $enabled_column_array ) ) {
                 return sprintf( '<p>' . esc_html( 'Table{ID: %s} Column Settings is not founded !', 'woo-extender-table' ). '</p>', $ID );
             }
             
             $column_array      = get_post_meta( $ID, 'wext_column_array', true );
             $column_settings   = get_post_meta( $ID, 'wext_column_settings', true );
             $basics            = get_post_meta( $ID, 'wext_basics', true );
             $conditions        = get_post_meta( $ID, 'wext_conditions', true );
             $pagination        = get_post_meta( $ID, 'wext_pagination', true );
            //  $config_value      = get_post_meta( $ID, 'wext_config', true );
             $search_filter     = get_post_meta( $ID, 'wext_search_n_filter', true );
             $config_value      = 263;//wext_get_config_value( $table_ID ); 

             if( is_array( $config_value ) ){
                array_unshift( $config_value, get_the_title( $ID ) ); //Added at V5.0
            }


             /**
             * Filter of common Array
             * @Hook Filter: wpto_column_array
             */

            $column_array = apply_filters( 'wexto_column_arr', $column_array, $table_ID, $atts, $column_settings, $enabled_column_array ); //Added at 6.0.25
            $enabled_column_array = apply_filters( 'wexto_enabled_column_array', $enabled_column_array, $table_ID, $atts, $column_settings, $column_array ); //Added at 6.0.25
            $column_settings = apply_filters( 'wexto_column_settings', $column_settings, $table_ID, $enabled_column_array ); //Added at 6.0.25


            /**
             * Product Type featue added for provide Variation Product table 
             * 
             * @since 5.7.7
             */
            $product_type = isset( $basics['product_type'] ) && !empty( $basics['product_type'] ) ? $basics['product_type'] : false;
            if( $product_type ){
                unset( $enabled_column_array['category'] );
                unset( $enabled_column_array['tags'] );
                unset( $enabled_column_array['weight'] );
                unset( $enabled_column_array['length'] );
                unset( $enabled_column_array['width'] );
                unset( $enabled_column_array['height'] );
                unset( $enabled_column_array['rating'] );
                unset( $enabled_column_array['attribute'] );
                unset( $enabled_column_array['variations'] );
            }


             //For Advance and normal Version
             $table_type = isset( $conditions['table_type'] ) ? $conditions['table_type'] : 'normal_table';//"advance_table"; //table_type
             if($table_type != 'normal_table'){
                 //unset( $enabled_column_array['price'] );
                 unset( $enabled_column_array['variations'] );
                 unset( $enabled_column_array['total'] );
                 unset( $enabled_column_array['quantity'] );
             }

            /**
             * Only for Message
             */
            if( isset( $enabled_column_array['message'] ) && $table_type != 'normal_table' ){
                /**
                 * For ThirdParty Plugin Support, We have
                 * Disable shortMesage from Column
                 * and added it into Single Product.
                 */
                unset( $enabled_column_array['message'] );
                add_action( 'woocommerce_before_add_to_cart_quantity', 'wext_add_custom_message_field' );
            }

            //Collumn Setting part
            $table_head = !isset( $basics['table_head'] ) ? true : false;

            $table_column_keywords = $enabled_column_array;

         } else {
             return false;
         }

         /**
         * Args for wp_query()
         */
        $args = array(
            'posts_per_page' => 2,
            'post_type' => array('product'),
            'post_status'   =>  'publish',
            'meta_query' => array(),
            'wext_query_type' => 'default',
        );

        //Table ID added to Args 
        $args['table_ID'] = $table_ID; //Added at V5.0


        ob_start();


        /**
         * To Insert Content at Top of the Table, Just inside of Wrapper tag of Table
         * Available Args $table_ID, $args, $config_value, $atts;
         */
        do_action( 'wext_action_start_table', $table_ID, $args, $column_settings, $enabled_column_array, $config_value, $atts );

        $html .= ob_get_clean();
      
        /**
         * To Show or Hide Table
         * Use following Filter
         */
        $table_show = apply_filters( 'wexto_table_show', true,  $table_ID, $args, $config_value, $atts );
      
        // if( !$table_show ){
        //     return $html;
        // }

        /**
         * Initialize Page Number
         */
        $page_number = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;

         /**
         * Do Detect Page number, When Table will be display.
         * 
         */
        $page_number = apply_filters( 'wext_page_number', $page_number, $table_ID, $args, $column_settings, $enabled_column_array, $column_array );
        $args['paged'] =( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : $page_number;

        /**
         * @Hook wpto_table_query_args to customize Query Args from any plugin.
         * Available Data/VAriable are: $args, $atts, $table_ID
         */
        $args = apply_filters( 'wext_table_query_args', $args, $table_ID, $atts, $column_settings, $enabled_column_array, $column_array );
        $responsive = 'wext-responsive';

          /**
         * Why this array here, Actually we will send this data as dataAttribute of table 's tag.
         * although function has called at bellow where this array need.
         */
        $table_row_generator_array = array(
            'args'                          => $args,
            'wext_table_column_keywords'    => $table_column_keywords,
            // 'texonomy_key'                  => $taxonomy_column_keywords,
            // 'customfield_key'               => $customfileds_column_keywords,
            // 'filter_key'                    => $filter_keywords,
            // 'filter_box'                    => $filter_box,
            // 'description_type'              => $description_type,
            // 'ajax_action'                   => $ajax_action,
            'table_type'                    => $table_type,
        );

        ob_start();

         /**
         * Action for before Table
         * @since 2.7.5.2
         */
        do_action( 'wexto_action_before_table', $table_ID, $args, $column_settings, $enabled_column_array, $config_value, $atts );
        $html .= ob_get_clean();

        $html .= '<div class="wpt_table_tag_wrapper">';
        $page_number_1plugs = $args['paged'] + 1;
        $temp_number = 211;
        $table_class_arr = array(
            $responsive,
            $table_type,
            'device_for_colum' . $_device,
            'wext_temporary_table_1',
            'wext_product_table',
            'template_table',
            "normal_table",
        );
        $table_class_arr = apply_filters( 'wexto_table_tag_class_arr', $table_class_arr, $table_ID, $args, $column_settings, $enabled_column_array, $column_array);
        $table_class_arr = implode( " ", $table_class_arr );

        $html .= "<table "
                . "data-page_number='" . esc_attr( $page_number_1plugs ) . "' "
                . "data-config_json='" . esc_attr( wp_json_encode( $config_value ) ) . "' "
                . "data-data_json='" . esc_attr( wp_json_encode( $table_row_generator_array ) ) . "' "
                . "data-data_json_backup='" . esc_attr( wp_json_encode( $table_row_generator_array ) ) . "' "
                . "id='" . apply_filters('wpt_change_table_id', 'wpt_table') . "' "
                . "class='{$table_class_arr}' "

                . ">";

        $responsive_table = "table#wext_table.mobile_responsive.wpt_temporary_table.wpt_product_table";

        $column_title_html = false;

        if ( $table_head && $enabled_column_array && is_array( $enabled_column_array ) && count( $enabled_column_array ) >= 1) {
            $column_title_html .= '<thead><tr data-temp_number="' . 12 . '" class="wpt_table_header_row wpt_table_head">';
            foreach ( $enabled_column_array as $key => $colunt_title ) {
                $updated_title = isset( $column_array[$key] ) ? $column_array[$key] : $colunt_title;
                $column_class = $key;

                $colunt_title = ( $column_class != 'check' ? $updated_title : "<input data-type='universal_checkbox' data-temp_number='{$temp_number}' class='wpt_check_universal' id='wpt_check_uncheck_column_{$temp_number}' type='checkbox'><label for=wpt_check_uncheck_column_{$temp_number}></label>" );
                $column_title_html .= "<th class='wpt_{$column_class}'>{$colunt_title}</th>";
            }
            $column_title_html .= '</tr></thead>';
            $html .= $column_title_html;
            $html .= '<tbody>'; //Starting TBody here

           $html .= wext_table_row_generator( $table_row_generator_array );

            $html .= '</tbody>'; //Tbody End here
            $html .= "</table>"; //Table tag wrapper End
            $html .= "</div>"; //End of .wpt-table-tag-wrapper

            ob_start();
             /**
         * Action for After Table
         * @since 2.7.5.2
         */
        do_action( 'wpto_action_after_table', $table_ID, $args, $column_settings, $enabled_column_array, $config_value, $atts );
        $html .= ob_get_clean();
        $html .= "</div>";
        return $html;
        }
    }
}



if( !function_exists( 'wext_table_row_generator' ) ){
    /**
     * Generate Table 's Root html based on Query args
     * 
     * @param type $args Query 's args
     * @param type $table_column_keywords table 's column
     * @param type $sort Its actually for Product Sorting
     * @param type $wpt_permitted_td Permission or each td
     * @param type $add_to_cart_text add_to_cart text
     * @return String 
     */
    function wext_table_row_generator( $table_row_generator_array ){
        ob_start();
        $html = false;
        //Getting WooProductTable Pro

        $table_ID = $table_row_generator_array['args']['table_ID'];
        // $config_value = wpt_get_config_value( $table_ID );
        // $_device = wpt_col_settingwise_device( $table_ID );
        // $basics = get_post_meta( $table_ID, 'basics', true );
        // $design = get_post_meta( $table_ID, 'table_style', true );

        $args                   = $table_row_generator_array['args'];
        $table_column_keywords = $table_row_generator_array['wext_table_column_keywords'];
        // $sort      = $table_row_generator_array['wpt_product_short'];
        // $wpt_permitted_td       = $table_row_generator_array['wpt_permitted_td'];
        // $add_to_cart_text   = $table_row_generator_array['wpt_add_to_cart_text'];
        $temp_number            = 211;
        // $texonomy_key           = $table_row_generator_array['texonomy_key'];//texonomy_key
        // $customfield_key        = $table_row_generator_array['customfield_key'];//texonomy_key
        // $filter_key             = $table_row_generator_array['filter_key'];//texonomy_key
        // $filter_box             = $table_row_generator_array['filter_box'];//Taxonomy Yes, or No
        // $description_type = $table_row_generator_array['description_type'];
        // $ajax_action            = $table_row_generator_array['ajax_action'];

        // $checkbox        = $table_row_generator_array['checkbox'];

        $table_type           = $table_row_generator_array['table_type'];

        // if( $args == false || $table_column_keywords == false ){
        //     return false;
        // }

        //WILL BE USE FOR EVERY WHERE INSIDE ITEM
        $column_array = get_post_meta( $table_ID, 'wext_column_array', true );
        $column_settings = get_post_meta( $table_ID, 'wext_column_settings' , true);
        
        /**
         * @Hook Filter: 
         * Here $table_column_keywords and $enabled_column_array are same Array Actually
         * in shortcode_generator function $atts - here null
         */
        $column_array = apply_filters( 'wexto_column_arr', $column_array, $table_ID, null, $column_settings, $table_column_keywords ); //Added at 2.9.8
        $column_settings = apply_filters( 'wexto_column_settings', $column_settings, $table_ID, $table_column_keywords ); //Added at 6.0.25 
        
        
        /**
         * Adding Filter for Args inside Row Generator
         */
        $args = apply_filters( 'wexto_table_query_args_in_row', $args, $table_ID, false, $column_settings, false, false );
       
        /**
         * Args fixer
         * 
         * @since 3.0.2.1
         */
        if(isset( $args['s'] ) && $args['s'] == 'false'){
            $args['s'] = false;
        }
        
        $args['posts_per_page'] = is_numeric( $args['posts_per_page'] ) ? (int) $args['posts_per_page'] : $args['posts_per_page'];
        
        $product_loop = new WP_Query($args);

        /**
         * If not set any Shorting (ASC/DESC) than Post loop will Random by Shuffle()
         * @since 1.0.0 -9
         */
        // if ($sort == 'random') {
        //     shuffle($product_loop->posts);
        // }
        
        $product_loop = apply_filters( 'wpto_product_loop', $product_loop, $table_ID, $args );
        
        $wpt_table_row_serial = (( $args['paged'] - 1) * $args['posts_per_page']) + 1; //For giving class id for each Row as well
        if (  $product_loop->have_posts() ) : while ($product_loop->have_posts()): $product_loop->the_post();
                global $product;

                $data = $product->get_data();
                $product_type = $product->get_type();
                $parent_id = $product->get_parent_id(); // Version 2.7.7
                
                (Int) $id = $data['id'];     

                $taxonomy_class = 'filter_row ';
                $data_tax = false;
                // if( $filter_box && is_array( $filter_key ) && count( $filter_key ) > 0 ){
                //     foreach( $filter_key as $tax_keyword){
                //         $terms = wp_get_post_terms( $data['id'], $tax_keyword  );

                //         $attr = "data-{$tax_keyword}=";

                //         $attr_value = false;
                //         if( is_array( $terms ) && count( $terms ) > 0 ){
                //             foreach( $terms as $term ){
                //                 $taxonomy_class .= $tax_keyword . '_' . $temp_number . '_' . $term->term_id . ' ';
                //                 $attr_value .= $term->term_id . ':' . $term->name . ', ';
                //             }
                //         }
                //         $data_tax .= $attr . '"' . $attr_value . '" ';
                //     }
                // }else{
                //    $taxonomy_class = 'no_filter'; 
                // }

                $default_quantity = apply_filters( 'woocommerce_quantity_input_min', 1, $product );

                $row_class = $data_product_variations = $variation_html = $wpt_varitions_col = $variable_for_total = false;
                $quote_class = 'enabled';

                if( $product->get_type() == 'variable' ){
                    /**
                     * $variable_for_total variable will use in Total colum. So we need just True false information
                     */
                    $variable_for_total = true;
                    $row_class = 'data_product_variations woocommerce-variation-add-to-cart variations_button woocommerce-variation-add-to-cart-disabled';
                    $quote_class = 'variations_button disabled';
                    $variable = new WC_Product_Variable( $data['id'] );

                    $available_variations = $variable->get_available_variations();
                    $data_product_variations = htmlspecialchars( wp_json_encode( $available_variations ) );


                    $attributes = $variable->get_variation_attributes();
                    $default_attributes = $variable->get_default_attributes(); //Added at 3.9.0
                    // $variation_html = wpt_variations_attribute_to_select( $attributes, $data['id'], $default_attributes, $temp_number );                 
                }


                //Out_of_stock class Variable
                $stock_status = $data['stock_status'];
                $stock_status_class = ( $stock_status == 'onbackorder' || $stock_status == 'instock' ? 'add_to_cart_button' : $stock_status . '_add_to_cart_button disabled' );

                $tr_class_arr = array(
                    "visible_row",
                    "wpt_row",
                    "wpt_row_" . $temp_number,
                    "wpt_row_serial_$wpt_table_row_serial",
                    "wpt_row_product_id_" . get_the_ID(),
                    "product_id_" . get_the_ID(),
                    $taxonomy_class,
                    $product_type,
                    "product_type_" . $product_type,
                    "stock_status_" . $data['stock_status'],
                    "backorders_" . $data['backorders'],
                    "sku_" . $data['sku'],
                    "status_" . $data['status'],
                    $product->is_sold_individually() ? "individually-sold" : "",

                );
                $tr_class_arr = apply_filters( 'wpto_tr_class_arr', $tr_class_arr, $args, $table_ID, $column_settings, $table_column_keywords, $product );
                $tr_class = implode( " ", $tr_class_arr );
                /**
                 * Table Row and
                 * And Table Data filed here will display
                 * Based on Query
                 */
                $wpt_each_row = false;

                do_action( 'wpto_before_row', $column_settings, $table_ID, $product, $temp_number );
                $row_manager_loc = WEXT_BASE_DIR . 'includes/row_manager.php';
                $row_manager_loc = apply_filters( 'wpo_row_manager_loc',$row_manager_loc, $column_settings,$table_column_keywords, $args, $table_ID, $product );
                if( file_exists( $row_manager_loc ) ){
                    include $row_manager_loc;
                }
                do_action( 'wpto_after_row', $column_settings, $table_ID, $product, $temp_number );

                $wpt_table_row_serial++; //Increasing Serial Number.

            endwhile;
            //Moved reset query from here to end of table at version 4.3
        else:
            //$html .= "<div class='wpt_loader_text wpt_product_not_found'>" . $config_value['product_not_founded'] . "</div>";
            ?>
                        <div class='wpt_product_not_found'><?php echo wp_kses_post( $config_value['product_not_founded'] ); ?></div>
            <?php
        endif;

        wp_reset_query(); //Added reset query before end Table just at Version 4.3

        $html = ob_get_clean();
        return $html;
    }
}