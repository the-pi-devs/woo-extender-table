<?php
/**
 * Plugin Name: Woo Extender Table
 * Plugin URI: https://gitlab.com/woo-extender-table/woo-extender-table/
 * Description: WooCommerce Table.
 * Version: 1.0.0
 * Author: Abdullah
 * Author URI: https://gitlab.com/masumtaf
 * Text Domain: woo-extender-table
 * Domain Path: /languages
 * License: GPL3
 */

// namespace Woo_Extender_Table;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */

if( !defined( 'VERSION' ) ){
    define( 'VERSION', '1.0.0' );
}

// Some Constants for ease of use
/**
 * Defining constant
 */
if( !defined( 'WEXT_PLUGIN_BASE_FOLDER' ) ){
    define( 'WEXT_PLUGIN_BASE_FOLDER', plugin_basename( dirname( __FILE__ ) ) );
}

if( !defined( 'WEXT_CAPABILITY' ) ){
    $wext_capability = apply_filters( 'wext_menu_capability', 'manage_wext_product_table' );
    define( 'WEXT_CAPABILITY', $wext_capability );
}

if( !defined( 'WEXT_PLUGIN' ) ){
    define( 'WEXT_PLUGIN', plugin_basename( __FILE__ ) ); //'woo-product-table/woo-product-table.php'
}

if( !defined( 'WEXT_PLUGIN_BASE_FILE' ) ){
    define( 'WEXT_PLUGIN_BASE_FILE', plugin_basename( __FILE__ ) ); //'woo-product-table/woo-product-table.php' )
}

if( !defined( 'WEXT_BASE_URL' ) ){
    define( "WEXT_BASE_URL", plugins_url() . '/'. plugin_basename( dirname( __FILE__ ) ) . '/' );
}

if( !defined( 'WEXT_DIR_BASE' ) ){
    define( "WEXT_DIR_BASE", dirname( __FILE__ ) . '/' );
}
if( !defined( 'WEXT_BASE_DIR' ) ){
    define( "WEXT_BASE_DIR", str_replace( '\\', '/', WEXT_DIR_BASE ) );
}

if( !defined( 'WEXT_PLUGIN_FOLDER_NAME' ) ){
    define( "WEXT_PLUGIN_FOLDER_NAME",plugin_basename( dirname( __FILE__ ) ) ); //aDDED TO NEW VERSION
}

if( !defined( 'WEXT_PLUGIN_FILE_NAME' ) ){
    define( "WEXT_PLUGIN_FILE_NAME", __FILE__ ); //aDDED TO NEW VERSION
}


/**
 * Default Configuration for WOO Product Table Pro
 * 
 * @since 1.0.0 -5
 */
$Ext_ShortCode_Text = 'Ext_Product_Table';

/**
* Including Plugin file for security
* Include_once
* 
* @since 1.0.0
*/
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

// Instance of the Woo_Extender_Table All In One class
Woo_Extender_Table::get_instance();

$column_array = array(
    'check'         => __( 'Check', 'woo-extender-table' ),
    'thumbnails'    => __( 'Thumbnails', 'woo-extender-table' ),
    'product_title' => __( 'Products', 'woo-extender-table' ),
    'category'      => __( 'Category', 'woo-extender-table' ),
    'tags'          => __( 'Tags', 'woo-extender-table' ),
    'sku'           => __( 'SKU', 'woo-extender-table' ),
    'weight'        => __( 'Weight(kg)', 'woo-extender-table' ),
    'length'        => __( 'Length(cm)', 'woo-extender-table' ),
    'width'         => __( 'Width(cm)', 'woo-extender-table' ),
    'height'        => __( 'Height(cm)', 'woo-extender-table' ),
    'rating'        => __( 'Rating', 'woo-extender-table' ),
    'stock'         => __( 'Stock', 'woo-extender-table' ),
    'price'         => __( 'Price', 'woo-extender-table' ),
    'wishlist'      => __( 'Wish List', 'woo-extender-table' ),
    'quantity'      => __( 'Quantity', 'woo-extender-table' ),
    'total'         => __( 'Total Price', 'woo-extender-table' ),
    'message'       => __( 'Short Message', 'woo-extender-table' ),
    'quick'         => __( 'Quick View', 'woo-extender-table' ),
    'date'          =>  __( 'Date', 'woo-extender-table' ),
    'modified_date' =>  __( 'Modified Date', 'woo-extender-table' ),
    'attribute'     =>  __( 'Attributes', 'woo-extender-table' ),
    'variations'    =>  __( 'Variations', 'woo-extender-table' ),
    'quoterequest'  => __( 'Quote Request', 'woo-extender-table' ),
    'description'   =>  __( 'Description', 'woo-extender-table' ), //has been removed at V5.2 //Again start at 6.0.25 //Again added
    'short_description'   =>  __( 'Short Description', 'woo-extender-table' ), //Added at v2.9.4
    'blank'         => __( 'Blank', 'woo-extender-table' ),
    'product_id'    => __( 'ID', 'woo-extender-table' ),
    'serial_number' => __( 'SL', 'woo-extender-table' ),
    'action'        => __( 'Action', 'woo-extender-table' ),
    'freeze' => __( 'Freeze Colum', 'woo-extender-table' ),
);
$column_array = apply_filters( 'wexto_default_column_arr', $column_array );

Woo_Extender_Table::$wext_columns_array =  $column_array;



$default_enabled_col_array = array(
    'check'         => 'check',  
    'thumbnails'    => 'thumbnails',  
    'product_title' => 'product_title',  
    'category'      => 'category',  
    'sku'           => 'sku',  
    'quantity'      => 'quantity',  
    'price'         => 'price',  
    'action'        => 'action',  
);

/**
 * Filter for Default Enabled Column
 * Available Args $default_enabled_col_array, $column_array
 */
$default_enabled_col_array = apply_filters( 'wexto_default_enable_column_arr', $default_enabled_col_array, $column_array );
Woo_Extender_Table::$wext_default_enable_columns_array =  $default_enabled_col_array;



/**
 * Woo Extender Table core class.
 *
 * Register plugin and make instances of core classes
 *
 * @package Woo Extender Table
 * @version 1.0.0
 * @since   1.0.0
 */
class Woo_Extender_Table {

    /**
     * Holds class instance
     *
     * @access private
     *
     * @package Woo Extender Table
     * @version 1.0.0
     */
    private static $instance;

    /**
     * List Of path
     * 
     * @since   1.0.0
     * @package Woo Extender Table
     * @version 1.0.0
     * @var Array
     */
    protected $paths = array();  

    /**
     * Set like Constant static array
     * Get this by getPath() method
     * Set this by setConstant() method
     *  
     * @var type array
     */
    private static $constant = array();

    /**
     * Set Like constant Static array
     * 
     * @since   1.0.0
     * @package Woo Extender Table
     * @version 1.0.0
     * @var Array
     */
    public static $default = array();      
    
    /**
     * Property for Shortcode Storting
     * 
     * @since   1.0.0
     * @package Woo Extender Table
     * @version 1.0.0
     * @var Array
     */
    public static $shortCode;   
    
    /**
     * Only for Admin Section, Collumn Array
     * 
     * @since   1.0.0
     * @package Woo Extender Table
     * @version 1.0.0
     * @var Array
     */
    public static $wext_columns_array = array();   
    
    /**
     * Only for Admin Section, Collumn Array
     * 
     * @since   1.0.0
     * @package Woo Extender Table
     * @version 1.0.0
     * @var Array
     */
    public static $wext_default_enable_columns_array = array();  

    /**
     * Only for Admin Section, Collumn Array
     * 
     * @since   1.0.0
     * @package Woo Extender Table
     * @version 1.0.0
     * @var Array
     */
    public static $wext_colums_disable_array = array();  
    
    /**
     * Only for Admin Section, Collumn Array
     * 
     * @since   1.0.0
     * @package Woo Extender Table
     * @version 1.0.0
     * @var Array
     */
    public static $wext_enable_columns_array = array();

    /**
     * Set Array for Style Form Section Options
     *
     * @since   1.0.0
     * @package Woo Extender Table
     * @version 1.0.0
     * @var Array
     */
    public static $wext_style_form_options = array();

        
    /**
    * Set Plugin Mode as 1 for Giving Data to UPdate Options
    *
    * @since   1.0.0
    * @package Woo Extender Table
    * @version 1.0.0
    * @var type Int
    */
    protected static $mode = 1;
    
    /**
     * Get class instance
     *
     * @since   1.0.0
     * @package Woo Extender Table
     * @version 1.0.0
     */
    public static function get_instance(){
        if( null === self::$instance ){
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Default constructor.
     *
     * Initialize plugin core and build environment
     *
     * @since   1.0.0
     * @package Woo Extender Table
     * @version 1.0.0
     */
    public function __construct() {

        $dir = dirname( __FILE__ ); //dirname( __FILE__ )

        /**
         * See $path_args for Set Path and set Constant
         * 
         * @since 1.0.0
         */
        $path_args = array(
            'PLUGIN_BASE_FOLDER'     =>  plugin_basename( $dir ),
            'PLUGIN_BASE_FILE'       =>  plugin_basename( __FILE__ ),
            'BASE_URL'               => trailingslashit( plugins_url( '',__FILE__) ),
            'BASE_DIR'               =>  str_replace( '\\', '/', $dir . '/' ),
        );

        /**
        * Set Path Full with Constant as Array
        * 
        * @since 1.0.0
        */
        $this->setPath($path_args);

        /**
        * Set Constant
        * 
        * @since 1.0.0
        */
        $this->setConstant($path_args);

        $this->install();
        $this->admin_includes();
        $this->includes();
        $this->add_actions();

    }

    
    /**
    * Set Path
    * 
    * @param type $path_array
    * 
    * @package Woo Extender Table
    * @version 1.0.0
    * @since   1.0.0
    */
    public function setPath( $path_array ) {
        $this->paths = $path_array;
    }

        
    private function setConstant( $contanst_array ) {
        self::$constant = $this->paths;
    }

    /**
    * Set Path as like Constant Will Return Full Path
    * Name should like Constant and full Capitalize
    * 
    * @param type $name
    * @return string
    */
    public function path( $name, $_complete_full_file_path = false ) {
        $path = $this->paths[$name] . $_complete_full_file_path;
        return $path;
    }

    /**
    * To Get Full path to Anywhere based on Constant

    * @package Woo Extender Table
    * @version 1.0.0
    * @since   1.0.0
    * @param type $constant_name
    * @return type String
    */
    public static function getPath( $constant_name = false ) {
        $path = self::$constant[$constant_name];
        return $path;
    }

    /**
     * Default admin Includes Files.
     *
     * Initialize plugin core and build environment
     *
     * @package Woo Extender Table
     * @version 1.0.0
     * @since   1.0.0
     * 
     */
    public function admin_includes() {

        include_once $this->path( 'BASE_DIR', 'admin/cpt-woo-extender-table.php');
        include_once $this->path( 'BASE_DIR', 'admin/metaboxes-table.php');
        include_once $this->path( 'BASE_DIR', 'admin/admin-enqueue.php');
        
    }     

    /**
     * All Action hookes.
     *
     * @package Woo Extender Table
     * @version 1.0.0
     * @since   1.0.0
     * 
     */
    public function add_actions() {
        
    }

    /**
     * All Files Includes Here.
     *
     * @package Woo Extender Table
     * @version 1.0.0
     * @since   1.0.0
     * 
     */
    public function includes() {
        include_once $this->path( 'BASE_DIR', 'includes/helper-functions.php');
        include_once $this->path( 'BASE_DIR', 'includes/shortcode.php');
      
    }

    /**
    * Update Options when Installing
    * This method has update at Version 3.6
    * 
    * @since 1.0.0
    * @updated since 3.6_29.10.2018 d/m/y
    */
    public static function install() {
            
        ob_start();
        //check current value

        $role = get_role( 'administrator' );

        $role->add_cap( 'edit_wext_product_table' );
        $role->add_cap( 'edit_wext_product_tables' );
        $role->add_cap( 'edit_others_wext_product_tables' );
        $role->add_cap( 'publish_wext_product_tables' );
        $role->add_cap( 'read_wext_product_table' );
        $role->add_cap( 'read_private_wext_product_tables' );
        $role->add_cap( 'delete_wext_product_table' );
        $role->add_cap( 'manage_wext_product_table' );


        //Configuration Data Update
        // $current_value = get_option('wext_configure_options');
        // $default_value = self::$default;
        // if( empty( $current_value ) ){
        //     update_option( 'wext_configure_options', $default_value );
        //     return;
        // }
        
        // if( is_array( $current_value ) && is_array( $default_value ) ){
        //     $updated = array_merge( $default_value, $current_value );
        //     update_option( 'wext_configure_options', $updated );
        //     return;
        // }
    }


}
