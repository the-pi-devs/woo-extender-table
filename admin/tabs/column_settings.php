<?php
$default_enable_array = Woo_Extender_Table::$wext_default_enable_columns_array;

$columns_array = Woo_Extender_Table::$wext_columns_array;

$for_add =  $meta_column_array = $updated_columns_array = get_post_meta( $post->ID, 'wext_column_array', true );

if( !$meta_column_array && empty( $meta_column_array ) ){
    $for_add = $updated_columns_array = Woo_Extender_Table::$wext_columns_array;
}
if( $updated_columns_array && !empty( $updated_columns_array ) && !empty( $columns_array ) ){
    $columns_array = array_merge( $columns_array, $updated_columns_array );
}

//var_dump(array_merge( $columns_array,$updated_columns_array ));
//unset($columns_array['description']); //Again Start Description Column From V6.0.25
$meta_enable_column_array = get_post_meta( $post->ID, 'wext_enabled_column_array', true );

if( $meta_enable_column_array && !empty( $meta_enable_column_array ) && !empty( $columns_array ) ){
    $columns_array = array_merge($meta_enable_column_array,$columns_array);
}

$column_settings = get_post_meta( $post->ID, 'wext_column_settings', true ); 
if( empty( $column_settings ) ){
    $column_settings = array();
}

$additional_collumn = array_diff(array_keys($for_add), array_keys( Woo_Extender_Table::$wext_columns_array ));

//var_dump($meta_enable_column_array,array_merge($meta_enable_column_array,$columns_array));

//var_dump( $meta_enable_column_array, $columns_array );
if( is_array( $meta_enable_column_array ) && !empty( $meta_enable_column_array ) ){
    //$columns_array = array_merge( $meta_enable_column_array, array_diff( $columns_array, $meta_enable_column_array ));
    $final_cols_arr = $meta_enable_column_array;
}else{
   $final_cols_arr = $default_enable_array; 
}

if( !is_array( $final_cols_arr ) ){
    return;
}


//$columns_array = array_merge($meta_enable_column_array,array_diff($columns_array,$meta_enable_column_array));
//var_dump($columns_array,$meta_enable_column_array);


//var_dump($updated_columns_array,$meta_enable_column_array);

/**
 * Some input name keyword as variable
 */
$enabled_column_array_name = 'wext_enabled_column_array';
$_device_name = '';
?>

<div class="inside-column-settings-wrapper">
    <div class="inside-column-setting-header">
        <h2><?php echo esc_html__( 'Devicewise Column Setting', 'woo-extender-table' ); ?></h2>
        <br>
    </div>
    
    
    <div id="inside-desktop" class="inside_tab_content tab-content tab-content-active">
<?php 
$enabled_column_array = $enabled_column_array_name;

$availe_column_list_file = __DIR__ . '/inc-column/available-column-list.php';
include $availe_column_list_file;


$column_list_file = __DIR__ . '/inc-column/column-list.php';
include $column_list_file;


?>
    </div>
<?php