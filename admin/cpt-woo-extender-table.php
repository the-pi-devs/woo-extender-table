<?php

if ( ! function_exists('wext_cpt_product_table_post') ) {

/**
 * Create Custom post type for Product Table. From now, we will store our shortcode or shortcode's value in this post as meta value
 * 
 * @since 4.1
 * @link https://codex.wordpress.org/Post_Types See details at WordPress.org about Custom Post Type
 */
function wext_cpt_product_table_post() {
        
	$labels = array(
		'name'                  => _x( 'Woo Table', 'Woo Table', 'woo-extender-table' ),
		'singular_name'         => _x( 'Woo TABLE', 'Woo TABLE', 'woo-extender-table' ),
		'menu_name'             => __( 'Woo TABLE', 'woo-extender-table' ),
		'name_admin_bar'        => __( 'Woo Table', 'woo-extender-table' ),
		'archives'              => __( 'Woo Table Archives', 'woo-extender-table' ),
		'attributes'            => __( 'Woo Table Attributes', 'woo-extender-table' ),
		'parent_item_colon'     => __( 'Parent Shortcode:', 'woo-extender-table' ),
		'all_items'             => __( 'Woo Table', 'woo-extender-table' ),
		'add_new_item'          => __( 'Add New', 'woo-extender-table' ),
		'add_new'               => __( 'Add New', 'woo-extender-table' ),
		'new_item'              => __( 'New Woo Table', 'woo-extender-table' ),
		'edit_item'             => __( 'Edit Woo Table', 'woo-extender-table' ),
		'update_item'           => __( 'Update Woo Table', 'woo-extender-table' ),
		'view_item'             => __( 'View Woo Table', 'woo-extender-table' ),
		'view_items'            => __( 'View Woo Tables', 'woo-extender-table' ),
		'search_items'          => __( 'Search Woo Table', 'woo-extender-table' ),
		'not_found'             => __( 'Not found', 'woo-extender-table' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'woo-extender-table' ),
		'featured_image'        => __( 'Featured Image', 'woo-extender-table' ),
		'set_featured_image'    => __( 'Set featured image', 'woo-extender-table' ),
		'remove_featured_image' => __( 'Remove featured image', 'woo-extender-table' ),
		'use_featured_image'    => __( 'Use as featured image', 'woo-extender-table' ),
		'insert_into_item'      => __( 'Insert into Woo Table', 'woo-extender-table' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Woo Table', 'woo-extender-table' ),
		'items_list'            => __( 'Woo Table list', 'woo-extender-table' ),
		'items_list_navigation' => __( 'Woo Table list navigation', 'woo-extender-table' ),
		'filter_items_list'     => __( 'Filter Woo Table list', 'woo-extender-table' ),
	);
	$args = array(
		'label'                 => __( 'WOO TABLE', 'woo-extender-table' ),
		'description'           => __( 'Generate your shortcode for Woo Table.', 'woo-extender-table' ),
		'labels'                => $labels,
		'supports'              => array('title'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
        'menu_icon'             => 'dashicons-cart',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
                'capability_type'       => 'post',
                'capabilities' => array(
                    'edit_post' => 'edit_wext_product_table',
                    'edit_posts' => 'edit_wext_product_tables',
                    'edit_others_posts' => 'edit_others_wext_product_tables',
                    'publish_posts' => 'publish_wext_product_tables',
                    'read_post' => 'read_wext_product_table',
                    'read_private_posts' => 'read_private_wext_product_tables',
                    'delete_post' => 'delete_wext_product_table',
                ),
                "rewrite" => [ "slug" => "wext_product_table", "with_front" => true ],
                // 'map_meta_cap' => true,
                'register_meta_box_cb'  => 'wext_shortcode_metaboxes',
	);
	register_post_type( 'wext_product_table', $args );

}
add_action( 'init', 'wext_cpt_product_table_post', 0 );

}

if( !function_exists( 'wext_shortcode_column_head' ) ){
    //Showing shortcode in All Shortcode page
    function wext_shortcode_column_head($default){
        if ( 'wext_product_table' == get_post_type() ){
        $default['wext_shortcode'] = "Shortcode";
        }
        return $default;
    }
}
add_filter('manage_posts_columns', 'wext_shortcode_column_head');

if( !function_exists( 'wext_shortcode_column_content' ) ){
    function wext_shortcode_column_content($column_name, $post_id){
        if ($column_name == 'wext_shortcode') {
            $post_title = get_the_title( $post_id );
            $post_title = preg_replace( '/[#$%^&*()+=\-\[\]\';,.\/{}|":<>?~\\\\]/',"$1", $post_title );
            echo "<input style='display: inline-block;width:300px;' class='wext_auto_select_n_copy' type='text' value=\"[Ext_Product_Table id='{$post_id}' name='{$post_title}']\" id='wext_shotcode_content_{$post_id}' readonly>";
            echo '<a style="font-size: 12px !important;padding: 4px 13px !important" class="button button-primary wext_copy_button_metabox" data-target_id="wext_shotcode_content_' . $post_id . '">'. esc_html__( 'Copy','woo-extender-table' ).'</a>';
            echo '<p style="color: green;font-weight:bold;display:none; padding-left: 12px;" class="wext_shotcode_content_' . $post_id . '"></p>';
        }  
    }
}
add_action('manage_posts_custom_column', 'wext_shortcode_column_content', 2, 2);


//Permalink Hiding Option
//add_filter( 'get_sample_permalink_html', 'wext_permalink_hiding' );
if( !function_exists( 'wext_permalink_hiding' ) ){
    function wext_permalink_hiding( $return ) {
        if ( 'wext_product_table' == get_post_type() ){
            $return = '';
        }
        return $return;
    }
}


//Hiding Preview Button from all shortcode page
//add_filter( 'page_row_actions', 'wext_preview_button_hiding', 10, 2 );
if( !function_exists( 'wext_preview_button_hiding' ) ){
    function wext_preview_button_hiding( $actions, $post ) {

        if ( 'wext_product_table' == get_post_type() ){
            unset( $actions['inline hide-if-no-js'] );
            unset( $actions['view'] );
        }
        return $actions;
    }
}
