(function($) {
    'use strict';
    $(document).ready(function() {

        $('select#ext_product_ids,select#product_tag_ids,select.ext_select2,select.internal_select,select.ua_select product_includes_excludes').on('select2:select', function(e){
            extSelectItem(e.target, e.params.data.id);
          });
  
          var removeAjax = function(aaa,bb){
                  $('.button,button').removeClass('ext_ajax_update');
              };
        //code for Sortable
        $( ".ext_column_sortable" ).sortable({
            handle:'.handle',
            stop: removeAjax,
        });
        $( ".checkbox_parent" ).sortable({
            handle:this,
            stop: removeAjax,
        });
        $( ".ext_responsive_each_wraper" ).sortable({handle:this});
        
        
        $(document).on('click','.colum_data_input',function(){
            var parents = $(this).parents('.ext_shortable_data');
            var onOff = parents.children('.extra_all_on_off');
            var extras = parents.children('.ext_column_setting_extra');
            extras.toggle('medium',function(){
                var status = $(this).attr('data-status');
                if(status == 'expanded'){
                    onOff.removeClass('off_now');
                    onOff.addClass('on_now');
                    onOff.html("Expand");
                    onOff.attr('data-status','expanded');
                    extras.parents('li').removeClass('expanded_li');
                    $(this).attr('data-status','collapsed');
                }else{
                    
                    onOff.removeClass('on_now');
                    onOff.addClass('off_now');
                    onOff.html("Collapse");
                    onOff.attr('data-status','collapsed');
                    extras.parents('li').addClass('expanded_li');
                    $(this).attr('data-status','expanded');
                }
                
            });
        });
        
        $(document).on('click','span.extra_all_on_off',function(){
            var key = $(this).data('key');
            var thisExpand = $(this);
            $('.ext_column_setting_extra.extra_all_' + key).toggle('medium',function(){
                var status = thisExpand.attr('data-status');
                if(status == 'expanded'){
                    thisExpand.removeClass('off_now');
                    thisExpand.addClass('on_now');
                    thisExpand.parents('li').removeClass('expanded_li');
                    thisExpand.html("Expand");
                    thisExpand.attr('data-status','collapsed');
                }else{
                    thisExpand.removeClass('on_now');
                    thisExpand.addClass('off_now');
                    thisExpand.parents('li').addClass('expanded_li');
                    thisExpand.html("Collapse");
                    thisExpand.attr('data-status','expanded');
                }
                
                
                
            });
            
        });
        $(document).on('click','ul#ext_column_sortable>li.ext_sortable_peritem.enabled .ext_column_arrow',function(){
            var target = $(this).attr('data-target');
            var keyword = $(this).attr('data-keyword');
            var thisElement = $(this).closest('li.ext_sortable_peritem.enabled');
            var prev = thisElement.prev();
            var prevClass = prev.attr('class');
            var next = thisElement.next();
            var nextClass = next.attr('class');
            console.log(target);
            //console.log(typeof prev, typeof next, typeof thisElement);
            if( target == 'next' && typeof next.html() !== 'undefined'){
                thisElement.before('<li class="' + nextClass + '">'+next.html()+'</li>');
                next.remove();
            }
            if( target == 'prev' && typeof prev.html() !== 'undefined'){
                thisElement.after('<li class="' + prevClass + '">'+prev.html()+'</li>');
                prev.remove();
            }
        });
        
        $('.ext_copy_button_metabox').click(function(){
            var ID_SELECTOR = $(this).data('target_id');
            copyMySelectedITem(ID_SELECTOR);
        });
          
        $('.ext_copy_button_metabox').click(function(){
            var ID_SELECTOR = $(this).data('target_id');
            copyMySelectedITem(ID_SELECTOR);
        });
        //ext_metabox_copy_content
        function copyMySelectedITem(ID_SELECTOR) {
          var copyText = document.getElementById(ID_SELECTOR);
          copyText.select();
          document.execCommand("copy");
          $('.' + ID_SELECTOR).html("Copied");
          $('.' + ID_SELECTOR).fadeIn();
          
          var myInterVal = setInterval(function(){
              $('.' + ID_SELECTOR).html("");
              $('.' + ID_SELECTOR).fadeOut();
              clearInterval(myInterVal);
          },1000);
        }

                
        /**
         * Inside Tab of Column
         * 
         * @type String
         */
         $('body').on('click','#ext_configuration_form .inside-column-settings-wrapper .inside-nav-tab-wrapper a', function(){
            $('.inside-nav-tab-wrapper a.nav-tab-active').removeClass('nav-tab-active');
            $(this).addClass('nav-tab-active');
            var target_tab = $(this).data('target');
            $('.inside-column-settings-wrapper .inside_tab_content.tab-content-active').removeClass('tab-content-active');
            $('.inside-column-settings-wrapper .inside_tab_content#'+target_tab).addClass('tab-content-active');
        });
        /**************Admin Panel's Setting Tab Start Here For Tab****************/
        var selectLinkTabSelector = "#ext_configuration_form a.ext_nav_tab";
        var selectTabContentSelector = "#ext_configuration_form .ext_tab_content";
        var selectLinkTab = $(selectLinkTabSelector);
        var selectTabContent = $(selectTabContentSelector);
        var tabName = window.location.hash.substr(1);
        if (tabName) {
            removingActiveClass();
            $('body #ext_configuration_form #' + tabName).addClass('tab-content-active');
            $('body #ext_configuration_form .nav-tab-wrapper a.ext_nav_tab.ext_nav_for_' + tabName).addClass('nav-tab-active');
        }
        
        $('body').on('click','#ext_configuration_form a.ext_nav_tab',function(e){
            e.preventDefault(); //Than prevent for click action of hash keyword
            var targetTabContent = $(this).data('tab');//getting data value from data-tab attribute
            
            // Detect if pushState is available
            if(history.pushState) {
              history.pushState(null, null, $(this).attr('href'));
            }
            removingActiveClass();
            $(this).addClass('nav-tab-active');
            $('body #ext_configuration_form #' + targetTabContent).addClass('tab-content-active');
            return false;
        });

        /**
         * Removing current active nav_tab and tab_content element
         * 
         * @returns {nothing}
         */
        function removingActiveClass() {
            selectLinkTab.removeClass('nav-tab-active');
            selectTabContent.removeClass('tab-content-active');
            return false;
        }


               
        /**
         * Managing Column from Activation Column List
         * 
         * @since We have added this featre at Version 2.7.8.2
         */
         $( 'body' ).on('click', '.add_switch_col_wrapper .switch-enable-available li.switch-enable-item', function(){
            var keyword = $(this).data('column_keyword');
            $(this).toggleClass('item-enabled');
            //Detect and set Responsive Stats
            ///detect_responsive_stats();
            $(this).closest('.tab-content').find('.ext_column_sortable li.ext_sortable_peritem input.checkbox_handle_input[data-column_keyword="' + keyword + '"]').trigger('click');
            
        });
        /**
         * Column Section Managing
         */
        $('body').on('click','.ext_column_sortable li.ext_sortable_peritem input.checkbox_handle_input',function(){
            var keyword = $(this).data('column_keyword');
            var thisextSortAble = $(this).closest('.ext_column_sortable');
            var targetLiSelector = thisextSortAble.find(' li.ext_sortable_peritem.column_keyword_' + keyword);
            
            if ($(this).prop('checked')) {
                //$(this).addClass('enabled');
                $(this).fadeIn('fast',function(){
                    $(this).addClass('enabled')
                }).css("display", "flex");
                //targetLiSelector.addClass('enabled');
                targetLiSelector.fadeIn('fast',function(){
                    targetLiSelector.addClass('enabled')
                }).css("display", "flex");
            } else {
                //Counting Column//
                var column_keyword;
                column_keyword = [];
                $('#inside-desktop .ext_column_sortable li.ext_sortable_peritem.enabled .ext_shortable_data input.colum_data_input').each(function(Index) {
                    column_keyword[Index] = $(this).data('keyword');
                });
                if (column_keyword.length < 2 && $(this).closest('.inside_tab_content').attr('id') === 'inside-desktop') {
                    alert('Minimum 1 column is required!');
                    return false;
                }
                //Counting colum End here
                
                
                
                //$(this).removeClass('enabled');
                $(this).fadeOut(function(){
                    $(this).removeClass('enabled');
                });
                $('.switch-enable-item-' + keyword).removeClass('item-enabled');
                //targetLiSelector.removeClass('enabled');
                targetLiSelector.fadeOut(function(){
                    targetLiSelector.removeClass('enabled');
                });
            }
//            
//            targetLiSelector.fadeIn(function(){
//                $(this).css('opacity','0.3');
//            });
        });
        
    });

    var myHtml = '<div class="wrapper_ext_ajax_update ultraaddons-button-wrapper">';
    myHtml += '<button type="submit" name="ext_post_submit" data-title="hello" class="stick_on_scroll button-primary button-primary primary button ext_ajax_update">Save Change</button>';
    myHtml += '</div>';
var colSetsLen = $('#column_settings').length;
if( colSetsLen > 0){
    $('#ext_configuration_form').append(myHtml);
}
$(window).on('scroll',function(){
    
    let targetElement = $('.stick_on_scroll');
    
    
    let bodyHeight = $('#wpbody').height();
    let scrollTop = $(this).scrollTop();
    let screenHeight = $(this).height();

    let configFormElement = $('#ext_configuration_form');
    if(configFormElement.length < 1) return;

    let conPass = bodyHeight - screenHeight - 300 - targetElement.height();
    let leftWill = configFormElement.width() - targetElement.width() - 20;
    

    targetElement.css({
        left: leftWill,
        right: 'unset'
    });
    if(scrollTop < conPass){
        targetElement.attr('id','stick_on_scroll-on');
    }else{
        targetElement.removeAttr('id');
    }
    
    if(scrollTop > 100 && colSetsLen > 0){
        targetElement.attr('id','stick_on_scroll-on');
    }else if(colSetsLen > 0){
        targetElement.removeAttr('id');
    }
    

});


   /**
         * Data Save by Ctrl + S
         */
    $(window).bind('keydown', function(event) {
        if (event.ctrlKey || event.metaKey) {
            if($('.form_bottom.form_bottom_submit_button').hasClass('wrapper_ext_ajax_update') && String.fromCharCode(event.which).toLowerCase() === 's' ){
                //Detect and set Responsive Stats
                ///detect_responsive_stats();
                
                event.preventDefault();
                $('body input#publish[name=save]').trigger('click');
            }
        }
    });


    $(document).on('change','li.ext_sortable_peritem.column_keyword_content',function(){
        $('.button,button').removeClass('ext_ajax_update');
    });
    
    $(document).on('click,change,focus','#ext-import-textarea',function(){
        $('.button,button').removeClass('ext_ajax_update');
    });
    

    //*********************** AJAX Save - AJAX data save
    $('body').append("<div class='ext_notify'><h1>Saving...</h1></div>");
    
    $(document).on('click','.checkbox_handle_input',function(){
        $('.button,button').removeClass('ext_ajax_update');
    });
    
    $(document).on('click','body .form_bottom.form_bottom_submit_button button.button.ext_ajax_update, body input#publish[name=save]',function(e){
        //Detect and set Responsive Stats
      
        
        $('.ext_notify').css('display','block');
        
        var 
        old_title   = $('button.button.ext_ajax_update').attr('data-title'), 
        new_title       = $('#title').val();
        
        if( old_title === new_title ){
            e.preventDefault();
        }else{
            return;
        }
        
        
        var postURL = 'post.php';
        
        
        //Collate all post form data
        var data = $('form#post').serializeArray();
        //Set a trigger for our save_post action
        data.push({ext_ajax_save: true});

        //The XHR Goodness
        $.post(postURL, data, function(response){}).done(function(){
            $('.ext_notify h1').html('Saved Product Table');
            $('.ext_notify').fadeOut();
        }).done(function() {
            var post_id = $('#post_ID').val();
            var ajax_url = WOE_DATA_ADMIN.ajax_url;
            
            $.ajax({
                type: 'POST',
                url: ajax_url,// + get_data,
                data: {
                    action:     'ext_set_post_meta',
                    post_id:    post_id,
                },
                complete: function(){
                    
                },
                success: function(data) {
                    $('#ext-export-textarea').html(data);
                },
                error: function() {
                    
                },
            });
            
        }).fail(function(){
            $('.ext_notify h1').html('Unable to Save, Please try again.');
            $('.ext_notify').fadeOut();
        });
        return false;

    });
    
})(jQuery);