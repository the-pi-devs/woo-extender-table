<?php
if( !function_exists( 'wext_admin_enqueue' ) ){
    /**
     * CSS or Style file add for Admin Section. 
     * 
     * @since 1.0.0
     * @update 1.0.3
     */
    function wext_admin_enqueue(){
        
        wp_enqueue_style( 'wext-admin', Woo_Extender_Table::getPath( 'BASE_URL' ) . 'admin/assets/css/admin.css', array(), VERSION, 'all' );

        wp_enqueue_script( 'wext-admin', Woo_Extender_Table::getPath( 'BASE_URL' ) . 'admin/assets/js/admin.js', array( 'jquery' ), VERSION, true );
        
        $ajax_url = admin_url( 'admin-ajax.php' );

        $is_pro = class_exists( 'WOO_Product_Table' ) ? 'yes' : 'no';
        $wext_DATA = array( 
           'ajaxurl' => $ajax_url,
           'ajax_url' => $ajax_url,

           );
        $wext_DATA = apply_filters( 'wexto_localize_data', $wext_DATA );
       wp_localize_script( 'wext-admin', 'wext_DATA_ADMIN', $wext_DATA );
    }
}
add_action( 'admin_enqueue_scripts', 'wext_admin_enqueue', 99 );
