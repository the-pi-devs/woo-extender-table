<?php
// namespace Woo_Extender_Table;
// use Woo_Extender_Table\Woo_Extender_Table;

/**
 * All metabox will control from here
 * This page added at 4.1.1 date: 19.1.2019
 * 
 * @since 4.1.1
 * @author Saiful Islam<codersaiful@gmail.com>
 */

if( !function_exists( 'wext_shortcode_metaboxes' ) ){
    /**
     * Our total metabox or register_meta_box_cb will controll from here. 
     * 
     * @since 4.1.1
     */
    function wext_shortcode_metaboxes(){
        add_meta_box( 'wext_shortcode_metabox_id', 'Shortcode', 'wext_shortcode_metabox_render', 'wext_product_table', 'normal' );
        add_meta_box( 'wext_shortcode_configuration_metabox_id', 'Table Configuration', 'wext_shortcode_configuration_metabox_render', 'wext_product_table', 'normal' ); 
    }

}



if( !function_exists( 'wext_shortcode_metabox_render' ) ){

    function wext_shortcode_metabox_render(){
        global $post;
        $curent_post_id = $post->ID;
        $post_title = preg_replace( '/[#$%^&*()+=\-\[\]\';,.\/{}|":<>?~\\\\]/',"$1", $post->post_title );
        echo '<div class="wext-shortcode-box-inside">';
        echo '<input type="text" value="[Ext_Product_Table id=\'' . $curent_post_id . '\' name=\'' . $post_title . '\']" class="wext_auto_select_n_copy wext_meta_box_shortcode mb-text-input mb-field" id="wext_metabox_copy_content" readonly>'; 
        echo '<a style="display:none;"  class="button button-primary wext_copy_button_metabox" data-target_id="wext_metabox_copy_content">Copy</a>';
        echo '<p style="color: #007692;font-weight:bold;display:none; padding-left: 12px;" class="wext_metabox_copy_content"></p>';
        echo '</div>';
        ?>
        <div class="wext-tips-github">
            <p>
                <b><?php echo esc_html__( 'Tips:', 'wext_pro' ); ?></b>
                <span>
                    <?php echo esc_html__( 'If you want to be a Contributor, Go to ', 'wext_pro' ); ?>
                    <a target="_blank" href="https://github.com/codersaiful/woo-product-table"><?php echo esc_html__( 'Github Repo', 'wext_pro' ); ?></a>.
                    | 
                    <?php echo esc_html__( 'Any Ideas? Please ', 'wext_pro' ); ?>
                    <a target="_blank" href="https://github.com/codersaiful/woo-product-table/discussions/new"><?php echo esc_html__( 'Send your Suggestion or Idea', 'wext_pro' ); ?></a>
                    
                </span>
            </p>
        </div>
        <?php
    }

}

if( !function_exists( 'wext_shortcode_configuration_metabox_render' ) ){
    //Now start metabox for shortcode Generator
    function wext_shortcode_configuration_metabox_render(){
        global $post;
        /**
         * Filter Hook was not working from theme's function file, so need this filter inside function
         */

        Woo_Extender_Table::$wext_columns_array = apply_filters( 'wexto_default_column_arr', Woo_Extender_Table::$wext_columns_array );
        Woo_Extender_Table::$wext_default_enable_columns_array = apply_filters( 'wexto_default_enable_column_arr', Woo_Extender_Table::$wext_default_enable_columns_array );
        
        echo '<input type="hidden" name="wext_shortcode_nonce_value" value="' . wp_create_nonce( plugin_basename( __FILE__ ) ) . '" />'; //We have to remove it later
        include __DIR__ . '/post-metabox-form.php';
        ?> 
        <br style="clear: both;">
        <?php
    }
}


if( !function_exists( 'wext_shortcode_configuration_metabox_save_meta' ) ){
    function wext_shortcode_configuration_metabox_save_meta( $post_id, $post ) { // save the data
        
        /*
        * We need to verify this came from our screen and with proper authorization,
        * because the save_post action can be triggered at other times.
        */

        if ( ! isset( $_POST['wext_shortcode_nonce_value'] ) ) { // Check if our nonce is set.
            return;
        }

        // verify this came from the our screen and with proper authorization,
        // because save_post can be triggered at other times
        if( ! wp_verify_nonce( $_POST['wext_shortcode_nonce_value'], plugin_basename(__FILE__) ) ) {
            return;
        }
        
        /**
         * @Hook Filter: wexto_on_save_global_post
         * To change/Modify $_POST
         * Before Save Data on Database by update_post_meta() func
         * @since 6.1.0.5
         * @Hook_Version: 6.1.0.5
         */

        $save_tab_array = array(
            'wext_column_array' => 'wext_column_array',
            'wext_enabled_column_array' => 'wext_enabled_column_array',
            'wext_column_settings' => 'wext_column_settings',
            'wext_basics' => 'wext_basics',
            'wext_table_style' => 'wext_table_style',
            'wext_conditions' => 'wext_conditions',
            'wext_mobile' => 'wext_mobile',
            'wext_search_n_filter' => 'wext_search_n_filter',
            'wext_pagination' => 'wext_pagination',
            'wext_config' => 'wext_config',
        );

        $save_tab_array = apply_filters( 'wext_save_tab_array', $save_tab_array, $post_id, $post );

        if( ! is_array( $save_tab_array ) || ( is_array( $save_tab_array ) && count( $save_tab_array ) < 1 )){
            return;
        }

        /**
         * @Hook Action: wexto_on_save_post_before_update_meta
         * To change data Just before update_post_meta() of our Product Table Form Data
         * @since 6.1.0.5
         * @Hook_Version: 6.1.0.5
         */
        add_action( 'wext_on_save_post_before_update_meta', $post_id );
        
        /**
         * In Filter, Availabe Tabs:
         * tabs: column_array,column_array_tablet,column_array_mobile,enabled_column_array,
         * enabled_column_array_tablet,enabled_column_array_mobile,
         * column_settings,column_settings_tablet,column_settings_mobile,
         * basics,table_style,conditions,mobile,search_n_filter,pagination,config
         * 
         * @since 2.9.1
         */
        $filtar_args = array(
            'wext_column_array' => array(
                'filter' => FILTER_SANITIZE_STRING,
                'flags' => FILTER_REQUIRE_ARRAY,
            ),
         
            'wext_enabled_column_array' => array(
                'filter' => FILTER_SANITIZE_STRING,
                'flags' => FILTER_REQUIRE_ARRAY,
            ),
         
            'wext_column_settings' => array(
                'filter' => FILTER_CALLBACK,
                'flags' => FILTER_REQUIRE_ARRAY,
                'options' => 'wp_kses_post'
            ),
            'wext_basics' => array(
                'filter' => FILTER_SANITIZE_STRING,
                'flags' => FILTER_REQUIRE_ARRAY,
            ),
            'wext_table_style' => array(
                'filter' => FILTER_SANITIZE_STRING,
                'flags' => FILTER_REQUIRE_ARRAY,
            ),
            'wext_conditions' => array(
                'filter' => FILTER_SANITIZE_STRING,
                'flags' => FILTER_REQUIRE_ARRAY,
            ),
            'wext_mobile' => array(
                'filter' => FILTER_SANITIZE_STRING,
                'flags' => FILTER_REQUIRE_ARRAY,
            ),
            'wext_search_n_filter' => array(
                'filter' => FILTER_SANITIZE_STRING,
                'flags' => FILTER_REQUIRE_ARRAY,
            ),
            'wext_pagination' => array(
                'filter' => FILTER_SANITIZE_STRING,
                'flags' => FILTER_REQUIRE_ARRAY,
            ),
            'wext_config' => array(
                'filter' => FILTER_SANITIZE_STRING,
                'flags' => FILTER_REQUIRE_ARRAY,
            ),
        );
        
        $submitte_data = filter_input_array( INPUT_POST, $filtar_args );
        $submitte_data = array_filter( $submitte_data );

        /**
         * To removed empty/false value from full array
         * currently it's inactivated
         * 
         * @since 1.0.4.1
         */
        //$submitte_data = wte_array_filter_recursive( $submitte_data );
        
        foreach( $save_tab_array as $tab ){
            
            /**
             * Already Filtered using filter_input_arry/filter_var_array
             * 
             * @since 2.9.1
             */
            $tab_data = isset( $submitte_data[$tab] ) ? $submitte_data[$tab] : false; //XSS OK
            
            /**
             * Hook before save tab data
             * @Hooked: wte_data_manipulation_on_save at admin/functions.php
             */
            $tab_data = apply_filters( 'wext_tab_data_on_save', $tab_data, $tab, $post_id, $save_tab_array );
            
            /**
             * Hook for Individual Tab data save.
             * 
             * Only for customer use at this moment.
             */
            $tab_data = apply_filters( 'wext_tab_data_on_save_' . $tab, $tab_data, $post_id, $save_tab_array );
            update_post_meta( $post_id, $tab, $tab_data );
        }

        /**
         * @Hook Action: wexto_on_save_post
         * To change data when Form will save.
         * @since 6.1.0.5
         * @Hook_Version: 6.1.0.5
         */
        add_action( 'wext_on_save_post', $post_id );

    }
}
add_action( 'save_post', 'wext_shortcode_configuration_metabox_save_meta', 10, 2 ); // 